import React from 'react'
import Item from './Item'

const Lists = ({list}) => {
  return (
    <div>
        <ul>{list.map((item) => <Item key={item.id} item={item}/>)}</ul>
    </div>
  )
}

export default Lists