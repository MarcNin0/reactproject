import React from 'react'

const Item = ({item: {name, description, company}}) => {
  return (
    <div>
        <h3>{name}</h3>
        <p>{description}</p>
        <span>Company: {company}</span>
    </div>
  )
}

export default Item