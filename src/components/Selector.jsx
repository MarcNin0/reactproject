import { useSelector } from "../hooks/useSelector";
import Lists from "./Lists";

const Selector = ({ url, option }) => {
  const { list, setChosen } = useSelector(url);

  return (
    <div>
      <select
        name="programming"
        id="programming"
        onChange={(e) => setChosen(e.target.value)}
      >
        <option value="">Select one</option>
        <option value={option}>{option}</option>
      </select>
      {list.length > 0 && <Lists list={list} />}
    </div>
  );
};

export default Selector;
