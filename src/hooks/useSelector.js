import { useState, useEffect } from "react";

export const useSelector = (url) => {
  const [chosen, setChosen] = useState("");
  const [list, setList] = useState([]);

  useEffect(() => {
    if (chosen) {
      fetch(`http://localhost:8081/${url}/listAll`)
        .then((res) => res.json())
        .then((info) => setList(info))
        .catch((error) => console.log(error));
    } else {
      setList([]);
    }
  }, [chosen]);

  return { setChosen, list };
};
