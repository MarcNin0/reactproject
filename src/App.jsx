import Selector from "./components/Selector";

function App() {
  const options = [
    {
      url: "frontend",
      option: "Front-end",
    },
    {
      url: "backend",
      option: "Back-end",
    },
    {
      url: "devOps",
      option: "DevOps",
    },
  ];

  return (
    <div>
      <h1>Software Development</h1>
      {options.map(({url, option}) => (
        <Selector key={url} url={url} option={option} />
      ))}
    </div>
  );
}

export default App;
