# README

Follow the steps below in order to start working on the project:

1) Click on the blue-button Clone > Clone with HTTPS 

2) Look for the place where you want to save your project and right click > Git Bash Here

3) When the Git Bash console opens up, code this command: git clone [copy the link from Clone with HTTPS]

4) Open your favourite IDE [Visual Code in my case] and open a new terminal. Type: "npm install" in order to install all the Node modules.

5) Start working on the project
